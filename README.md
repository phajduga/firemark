# Firemark - web based rpg/adventure/riddle game

## Contents of the repository

* **/webapp** - directory containing all the static content for the SPA

* **/framework** - utilities, resources, common code

* **/api** - api routings and resources

* **/config** - configuration .ini files

* **/tests** - unittests TBD

## Running

### Initialization

    $ ./firemark init

### Development server

To run the development server at localhost:8880:

    $ ./firemark run
